const { listOfPosts } = require("./posts");

const getSum = (str1, str2) => {
  if( typeof str1 !== 'string' || 
      typeof str2 !== 'string' ||
      isNaN(+str1) || 
      isNaN(+str2) ){

    return false
  } else {
    return String(+str1 + +str2)
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post = 0;
  let comment = 0;

  listOfPosts.forEach(item => {
    if(item.hasOwnProperty('post')){
      if(authorName === item.author) {
        post++
      }
    }

    if(item.hasOwnProperty('comments')) {
      item.comments.forEach(element => {
        if(authorName === element.author) {
          comment++
        }
      })
    }
  })

  return `Post:${post},comments:${comment}`
};

const tickets=(people)=> {
  const res = people.reduce((acc, curr) => {

    const change = curr - 25

    if(change > acc) {
      return
    } else {
      return acc += 25
    }
  }, 0)

  return isNaN(res) ? 'NO' : 'YES'
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
